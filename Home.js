import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';

const Tombol = ({label, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={{color:'black'}}>{label}</Text>
    </TouchableOpacity>
  );
};

export class Home extends Component {
  render() {
    return (
      <View style={styles.page}>
        <Tombol
          label={'Part Satu'}
          onPress={() => this.props.navigation.navigate('Part Satu')}
        />
        <Tombol label={'Part Dua'} />
        <Tombol label={'Part Tiga'} />
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    margin: 10,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'pink',
  },

});