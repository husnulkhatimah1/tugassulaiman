import { View, StyleSheet } from 'react-native';
import * as React from 'react';


export default function App () {
  return (
    <View style={{ flex:1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center'
    }}>
      <View style={{height:50, width:50, backgroundColor:'gray'}} />
      <View style={{height:50, width:50, backgroundColor:'green'}} />
      <View style={{height:50, width:50, backgroundColor:'pink'}} />
      <View style={{height:50, width:50, backgroundColor:'khaki'}} />
    </View>
  );
}
