import React, {useEffect} from 'react' ;
import {Image, StyleSheet, Text, View} from 'react-native';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('tabsnavigations');
        }, 3000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={require('../b/splash.png')} style={styles.logo} />
        </View>
    );
};
export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: 'ghostwhite',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo : {
        margin : 10,
        width : 350,
        height : 350,
    },
    welcomeText: {
        fontSize: 50,
        fontWeight: 'bold',
        color: 'black',
        paddingBottom: 20,
    },
});