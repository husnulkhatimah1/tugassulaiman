import { View, Text, TextInput } from 'react-native';
import React, { useState} from 'react';
import { Styles } from '../Styles';

export default function HelloFriends(props) {
  const [angka, setAngka] = useState(3);
  
  const ubahState = (teks) =>{
    
    setAngka(teks);
  }
return (
    <View style={Styles.container}>
      <TextInput
      style={Styles.input}
      placeholder="Apa Angka Favoritmu?"
      onChangeText={ubahState}
      />
      <Text style={Styles.teks}>Hello {props.nama}</Text>
      <Text style={Styles.teks}>Angka Favoritmu {angka}</Text>
    </View>
  );
}