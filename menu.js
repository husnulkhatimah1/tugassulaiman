import { View, Text, Image, StyleSheet } from 'react-native';
import React from 'react';

export default function MenuItem(props) {
  return (
    <View style={styles.item}>
      <Image source={props.image} style={styles.gambar} />
    </View>
  );
}

const styles= StyleSheet.create({
    item :{
        height: 250,
        width: 250,
        padding: 30,
        paddingTop: 5,
    },
    gambar :{
        height: 150,
        width: 150,
       
    }
})