import { View, Text, Image, StyleSheet } from 'react-native';
import React from 'react';

export default function Teknik(props) {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{uri:props.gambar}} />
      <View style={styles.teks}>
        <Text style={styles.title}>{props.judul}</Text>
        <Text style={styles.title1}>{props.pengertian}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    container :{
        height:130,
        flexDirection:'row',
        alignItems:'center',
        borderBottomWidth:3,
        borderColor: 'grey',
    },
    image: {
        margin: 20,
        width: 50,
        height: 50,
        borderRadius: 50/2
    },
    teks : { flex : 1},
    title: {
        fontWeight:'bold',
        fontSize: 20
    },
    title1: {
        fontSize : 20,
        color: 'red'
    }
});