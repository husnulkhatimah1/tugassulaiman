import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
    container :{    flex:1, 
    padding:15, 
    margin:50
    },
    teks: {
        marginTop: 15,
      paddingVertical: 30,
      borderWidth: 12,
      borderRadius: 8,
      backgroundColor: "grey",
      textAlign: "center",
      fontSize: 25,
      fontWeight: "bold"
    },
    input:{
      marginTop: 15,
      paddingVertical: 30,
      borderWidth: 10,
      borderRadius: 3,
      textAlign: "center",
      fontSize: 25,
      fontWeight: "bold"
    }

});
export { Styles };